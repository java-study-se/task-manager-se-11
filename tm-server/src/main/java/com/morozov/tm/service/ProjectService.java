package com.morozov.tm.service;

import com.morozov.tm.api.IProjectRepository;
import com.morozov.tm.entity.Project;
import com.morozov.tm.enumerated.StatusEnum;
import com.morozov.tm.exception.ProjectNotFoundException;
import com.morozov.tm.exception.RepositoryEmptyException;
import com.morozov.tm.exception.StringEmptyException;
import com.morozov.tm.util.ConsoleHelperUtil;
import com.morozov.tm.util.DataValidationUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

public final class ProjectService implements com.morozov.tm.api.IProjectService {

    final private IProjectRepository projectRepository;

    public ProjectService(@NotNull IProjectRepository ProjectRepository) {
        this.projectRepository = ProjectRepository;
    }

    @Override
    @NotNull
    public final List<Project> findAllProject(){
        @NotNull final List<Project> projectList = projectRepository.findAll();
        return projectList;
    }

    @Override
    @NotNull
    public final List<Project> getAllProjectByUserId(@NotNull String userId) throws RepositoryEmptyException {
        @NotNull final List<Project> projectListByUserId = projectRepository.findAllByUserId(userId);
        DataValidationUtil.checkEmptyRepository(projectListByUserId);
        return projectListByUserId;
    }
    @NotNull
    @Override
    public final Project addProject(@NotNull final String userId, @NotNull final String projectName)
            throws StringEmptyException {
        DataValidationUtil.checkEmptyString(projectName);
        @NotNull final Project project = new Project();
        project.setName(projectName);
        project.setUserId(userId);
        projectRepository.persist(project.getId(), project);
        return project;
    }

    @Override
    public final boolean removeProjectById(@NotNull final String userId, @NotNull String id) throws StringEmptyException {
        boolean isDeleted = false;
        DataValidationUtil.checkEmptyString(id);
        if (projectRepository.findOneByUserId(userId, id) != null) {
            projectRepository.remove(id);
            isDeleted = true;
        }
        return isDeleted;
    }

    @Override
    public final void updateProject(@NotNull final String userId, @NotNull final String id, @NotNull final String projectName,
            @NotNull final String projectDescription, @NotNull final String dataStart, @NotNull final String dataEnd)
            throws RepositoryEmptyException, StringEmptyException, ParseException, ProjectNotFoundException {
        DataValidationUtil.checkEmptyRepository(projectRepository.findAllByUserId(userId));
        DataValidationUtil.checkEmptyString(id, projectName, projectDescription);
        final Project project = (Project) projectRepository.findOne(id);
        if(project == null) throw new ProjectNotFoundException();
        project.setId(id);
        project.setName(projectName);
        project.setDescription(projectDescription);
        final Date updatedStartDate = ConsoleHelperUtil.formattedData(dataStart);
        if (updatedStartDate != null) {
            project.setStatus(StatusEnum.PROGRESS);
        }
        project.setStartDate(updatedStartDate);
        final Date updatedEndDate = ConsoleHelperUtil.formattedData(dataEnd);
        if (updatedEndDate != null) {
            project.setStatus(StatusEnum.READY);
        }
        project.setEndDate(updatedEndDate);
        project.setUserId(userId);
        projectRepository.merge(id, project);
    }

    @Override
    @NotNull
    public final List<Project> findProjectByStringInNameOrDescription(@NotNull final String userId,@NotNull final String string)
            throws RepositoryEmptyException {
        if (string.isEmpty()) return projectRepository.findAllByUserId(userId);
        @NotNull final List<Project> projectListByUserId = projectRepository.findProjectByStringInNameOrDescription(userId, string);
        DataValidationUtil.checkEmptyRepository(projectListByUserId);
        return projectListByUserId;
    }

    @Override
    public void removeAllByUserId(@NotNull final String userId) {
        projectRepository.removeAllByUserId(userId);
    }

    @Override
    public final void clearProjectList() {
        projectRepository.removeAll();
    }

    @Override
    public void loadProjectList(@Nullable List<Project> loadProjectList) {
        if (loadProjectList != null) {
            projectRepository.load(loadProjectList);
        }
    }

}
