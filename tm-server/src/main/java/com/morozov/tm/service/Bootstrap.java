package com.morozov.tm.service;

import com.morozov.tm.api.*;

import com.morozov.tm.entity.User;
import com.morozov.tm.repository.ProjectRepository;
import com.morozov.tm.repository.SessionRepository;
import com.morozov.tm.repository.TaskRepository;
import com.morozov.tm.repository.UserRepository;
import com.morozov.tm.util.EndpointPublishUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;


public final class Bootstrap implements IServiceLocator {
    @NotNull
    final private IProjectService projectService = new ProjectService(new ProjectRepository());
    @NotNull
    final private ITaskService taskService = new TaskService(new TaskRepository());
    @NotNull
    final private IUserService userService = new UserService(new UserRepository());
    @NotNull
    final private IDomainService domainService = new DomainService(this);
    @NotNull
    final private ISessionService sessionService = new SessionService(new SessionRepository());

    public void init() {
        EndpointPublishUtil.publish(this);
    }

    @NotNull
    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @NotNull
    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @NotNull
    @Override
    public IUserService getUserService() {
        return userService;
    }

    @NotNull
    @Override
    public IDomainService getDomainService() {
        return domainService;
    }

    @NotNull
    @Override
    public ISessionService getSessionService() {
        return sessionService;
    }


}

