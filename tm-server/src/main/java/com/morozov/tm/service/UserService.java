package com.morozov.tm.service;

import com.morozov.tm.api.IUserRepository;
import com.morozov.tm.entity.User;
import com.morozov.tm.enumerated.UserRoleEnum;
import com.morozov.tm.exception.StringEmptyException;
import com.morozov.tm.exception.UserExistException;
import com.morozov.tm.exception.UserNotFoundException;
import com.morozov.tm.util.DataValidationUtil;
import com.morozov.tm.util.MD5HashUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public final class UserService implements com.morozov.tm.api.IUserService {

    final private IUserRepository userRepository;

    public UserService(@NotNull final IUserRepository UserRepository) {
        this.userRepository = UserRepository;
    }
    @NotNull
    @Override
    public User loginUser(@NotNull final String login, @NotNull final String password)
        throws UserNotFoundException, StringEmptyException {
        DataValidationUtil.checkEmptyString(login, password);
        @Nullable final User user = (User) userRepository.findOneByLogin(login);
        if (user == null) throw new UserNotFoundException();
        final String typePasswordHash = MD5HashUtil.getHash(password);
        if (typePasswordHash == null) throw new UserNotFoundException();
        if (!typePasswordHash.equals(user.getPasswordHash())) throw new UserNotFoundException();
            return user;
    }
    @NotNull
    @Override
    public String registryUser(@NotNull final String login, @NotNull final String password)
        throws UserExistException, StringEmptyException {
        DataValidationUtil.checkEmptyString(login, password);
        if (userRepository.findOneByLogin(login) != null) throw new UserExistException();
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setRole(UserRoleEnum.USER);
        final String hashPassword = MD5HashUtil.getHash(password);
        if (hashPassword == null) throw new StringEmptyException();
        user.setPasswordHash(hashPassword);
        @NotNull final String userId = user.getId();
        userRepository.persist(userId, user);
        return userId;
    }


    @Override
    public void updateUserPassword(@NotNull final String id, @NotNull final String newPassword) throws StringEmptyException, UserNotFoundException {
        @Nullable final User user = (User) userRepository.findOne(id);
        DataValidationUtil.checkEmptyString(newPassword);
        if (user == null) throw new UserNotFoundException();
        String hashNewPassword = MD5HashUtil.getHash(newPassword);
        if (hashNewPassword != null) {
            user.setPasswordHash(hashNewPassword);
        }
        userRepository.merge(id, user);
    }

    @Override
    public void updateUserProfile(@NotNull final String id, @NotNull final String newUserName, @NotNull final String newUserPassword)
        throws StringEmptyException, UserExistException, UserNotFoundException {
        @Nullable final User user = (User) userRepository.findOne(id);
        DataValidationUtil.checkEmptyString(newUserName, newUserPassword);
        if (userRepository.findOneByLogin(newUserName) != null) throw new UserExistException();
        if (user == null) throw new UserNotFoundException();
        user.setLogin(newUserName);
        String hashNewUserPassword = MD5HashUtil.getHash(newUserPassword);
        if (hashNewUserPassword != null) {
            user.setPasswordHash(hashNewUserPassword);
        }
        userRepository.merge(id, user);
    }
    public void loadUserList(@Nullable final List<User> userList){
        if (userList != null) {
            userRepository.load(userList);
        }
    }

    @Override
    public @NotNull List<User> findAllUser() {
        return userRepository.findAll();
    }

    @Override
    public @NotNull User findOneById(@NotNull String id) throws UserNotFoundException {
        User user = (User) userRepository.findOne(id);
        if(user == null) throw new UserNotFoundException();
        return user;
    }
}
