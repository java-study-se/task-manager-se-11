package com.morozov.tm.repository;

import com.morozov.tm.api.IRepository;
import com.morozov.tm.entity.AbstractEntity;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class AbstractRepository<T extends AbstractEntity> implements IRepository<T> {
    @NotNull
    final Map<String, T> entityMap = new HashMap<>();

    @NotNull
    @Override
    public List<T> findAll() {
        return new ArrayList<>(entityMap.values());
    }

    @Override
    @Nullable
    public T findOne(@NotNull String id) {
        return entityMap.get(id);
    }

    @Override
    public void merge(@NotNull String id, @NotNull T updateEntity) {
        entityMap.put(id, updateEntity);
    }

    @Override
    public void persist(@NotNull String id, @NotNull T writeEntity) {
        entityMap.put(id, writeEntity);
    }

    @Override
    public void remove(@NotNull String id) {
        entityMap.remove(id);
    }

    @Override
    public void removeAll() {
        entityMap.clear();
    }

    @Override
    public void load(@NotNull List<T> loadEntityList) {
        for (@NotNull final T entity: loadEntityList) {
            entityMap.put(entity.getId(), entity);
        }
    }
}
