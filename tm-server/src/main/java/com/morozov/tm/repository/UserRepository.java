package com.morozov.tm.repository;

import com.morozov.tm.api.IUserRepository;
import com.morozov.tm.entity.User;
import com.morozov.tm.enumerated.UserRoleEnum;
import com.morozov.tm.util.MD5HashUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository<User> {

    public UserRepository() {
        @NotNull final User admin = new User();
        admin.setLogin("admin");
        @Nullable final String adminPasswordHash = MD5HashUtil.getHash("admin");
        if (adminPasswordHash == null) return;
        admin.setPasswordHash(adminPasswordHash);
        admin.setRole(UserRoleEnum.ADMIN);
        @NotNull final User user = new User();
        user.setLogin("user");
        @Nullable final String userPasswordHash = MD5HashUtil.getHash("user");
        if (userPasswordHash == null) return;
        user.setPasswordHash(userPasswordHash);
        user.setRole(UserRoleEnum.USER);
        entityMap.put(admin.getId(), admin);
        entityMap.put(user.getId(), user);
    }

    @Nullable
    @Override
    public User findOneByLogin(@NotNull final String login) {
        @NotNull final List<User> userList = findAll();
        User foundUser = null;
        for (@NotNull final User user : userList) {
            if (user.getLogin().equals(login)) foundUser = user;
        }
        return foundUser;
    }

}



