package com.morozov.tm.util.comparator;

import com.morozov.tm.entity.AbstractWorkEntity;
import org.jetbrains.annotations.NotNull;

import java.util.Comparator;

public class DataEndComparator implements Comparator<AbstractWorkEntity> {
    @Override
    public int compare(@NotNull final AbstractWorkEntity o1, @NotNull final AbstractWorkEntity o2) {
        if (o1.getEndDate() == null && o2.getEndDate() == null) return 0;
        if (o1.getEndDate() != null && o2.getEndDate() == null) return -1;
        if (o1.getEndDate() == null && o2.getEndDate() != null) return 1;
        return o1.getEndDate().compareTo(o2.getEndDate());
    }
}
