package com.morozov.tm.util;

import com.morozov.tm.exception.RepositoryEmptyException;
import com.morozov.tm.exception.StringEmptyException;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public class DataValidationUtil {
    public static void checkEmptyRepository(@NotNull final List repository) throws RepositoryEmptyException {
        if (repository.isEmpty()) throw new RepositoryEmptyException();
    }
    public static void checkEmptyString(@NotNull final String... strings) throws StringEmptyException {
        for (@NotNull String string: strings) {
            if (string.isEmpty())throw new StringEmptyException();
        }
    }
}
