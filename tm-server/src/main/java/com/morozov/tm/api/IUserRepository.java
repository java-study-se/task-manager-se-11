package com.morozov.tm.api;

import com.morozov.tm.entity.AbstractEntity;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IUserRepository<T extends AbstractEntity> extends IRepository<T> {
    @Nullable
    T findOneByLogin(@NotNull final String login);
}

