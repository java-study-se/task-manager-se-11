package com.morozov.tm.api;

import com.morozov.tm.entity.Session;
import com.morozov.tm.entity.User;
import com.morozov.tm.exception.AccessFirbidenException;
import org.jetbrains.annotations.NotNull;

public interface ISessionService {

    Session getNewSession(final User user);

    @NotNull
    Session validate(final Session session) throws AccessFirbidenException, CloneNotSupportedException;

    void closeSession(@NotNull final Session session);
}
