package com.morozov.tm.api;

import com.morozov.tm.entity.AbstractWorkEntity;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public interface ITaskRepository<T extends AbstractWorkEntity> extends IRepository<T> {

    List<T> findAllByProjectIdUserId(@NotNull final String userId, @NotNull final String projectId);

    List<T> findAllByUserId(@NotNull final String userId);

    T findOneByUserId(@NotNull final String userId, @NotNull final String id);

    List<T> getAllTaskByProjectId(@NotNull final String projectId);

    void deleteAllTaskByProjectId(@NotNull final String userId, @NotNull final String projectId);

    List<T> findTaskByStringInNameOrDescription(@NotNull final String userId, @NotNull final String string);

    void removeAllByUserId(@NotNull final String userId);
}
