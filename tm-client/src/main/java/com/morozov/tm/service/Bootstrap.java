package com.morozov.tm.service;

import com.morozov.tm.api.IServiceLocator;
import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.endpoint.*;
import com.morozov.tm.enumerated.UserRoleEnum;
import com.morozov.tm.exception.CommandCorruptException;
import com.morozov.tm.util.ConsoleHelperUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;

public final class Bootstrap implements IServiceLocator {
    @NotNull
    final private Map<String, AbstractCommand> commands = new TreeMap<>();
    @Nullable
    private Session session = null;
    @NotNull
    final private DomainEndpoint domainEndpoint = new DomainEndpointService().getDomainEndpointPort();
    @NotNull
    final private UserEndpoint userEndpoint = new UserEndpointService().getUserEndpointPort();
    @NotNull
    final private ProjectEndpoint projectEndpoint = new ProjectEndpointService().getProjectEndpointPort();
    @NotNull
    final private TaskEndpoint taskEndpoint = new TaskEndpointService().getTaskEndpointPort();
    @NotNull
    final private SessionEndpoint sessionEndpoint = new SessionEndpointService().getSessionEndpointPort();


    public void init(@NotNull Set<Class<? extends AbstractCommand>> classes) {
        ConsoleHelperUtil.writeString("Вас приветствует программа Task Manager. " +
                "Наберите \"help\" для вывода списка доступных команд. ");
        @NotNull String console = "";
        while (!"exit".equals(console)) {
            commands.clear();
            try {
                fillCommands(classes);
                console = ConsoleHelperUtil.readString();
                AbstractCommand command = commands.get(console);
                if (command != null) {
                    command.execute();
                }
            } catch (Exception e) {
                ConsoleHelperUtil.writeString(e.getMessage());
            }
        }
    }

    @NotNull
    @Override
    public UserEndpoint getUserEndpoint() {
        return userEndpoint;
    }

    @NotNull
    @Override
    public TaskEndpoint getTaskEndpoint() {
        return taskEndpoint;
    }

    @NotNull
    @Override
    public ProjectEndpoint getProjectEndpoint() {
        return projectEndpoint;
    }

    @NotNull
    @Override
    public DomainEndpoint getDomainEndpoint() {
        return domainEndpoint;
    }

    @Override
    public @Nullable Session getSession() {
        return session;
    }

    @Override
    public void setSession(@Nullable Session session) {
        this.session = session;
    }

    @Override
    public @NotNull SessionEndpoint getSessionEndpoint() {
        return sessionEndpoint;
    }

    @Override
    public List<AbstractCommand> getCommandList() {
        return new ArrayList<>(commands.values());
    }

    private void registry(@NotNull final AbstractCommand command) throws CommandCorruptException {
        @Nullable final String cliCommand = command.getName();
        @Nullable final String cliDescription = command.getDescription();
        if (cliCommand == null || cliCommand.isEmpty())
            throw new CommandCorruptException();
        if (cliDescription == null || cliDescription.isEmpty())
            throw new CommandCorruptException();
        command.setServiceLocator(this);
        commands.put(cliCommand, command);
    }

    private void fillCommands(@NotNull Set<Class<? extends AbstractCommand>> classes) throws CommandCorruptException, ClassNotFoundException, IllegalAccessException, InstantiationException {
        for (Class clazz : classes) {
            @NotNull AbstractCommand command = (AbstractCommand) Class.forName(clazz.getName()).newInstance();
            if (command == null) return;
            registryAllUserCommand(command);
            if (session != null) {
                registryUserSessionCommand(command);
            } else {
                registryEmptyUserSessionCommand(command);
            }

        }
    }

    private void registryAllUserCommand(@NotNull AbstractCommand command) throws CommandCorruptException {
        if (command.getUserRoleList().contains(UserRoleEnum.ALL)) registry(command);
    }

    private void registryEmptyUserSessionCommand(@NotNull AbstractCommand command) throws CommandCorruptException {
        if (command.getUserRoleList().isEmpty()) registry(command);
    }

    private void registryUserSessionCommand(@NotNull AbstractCommand command) throws CommandCorruptException {
        if (session == null) return;
        if (session.getUserRoleEnum().value().equals("ADMIN")) {
            if (command.getUserRoleList().contains(UserRoleEnum.ADMIN)) {
                registry(command);
            }
        }
        if(session.getUserRoleEnum().value().equals("USER")){
            if (command.getUserRoleList().contains(UserRoleEnum.USER)) {
                registry(command);
            }
        }
    }
}

